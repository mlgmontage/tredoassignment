## Tredo assignment

Some animations is not implemented since I didn't had enough time for this.

I chose feature sliced design since its great for big frontend applications. Also design made with vanilla css without any frameworks and strictly uses BEM and compositional approach like in tailwindcss.

- Architecture - `feature sliced design`
- UI - `vue`
- State manager - `vuex`

import { createStore } from "vuex";
import userModule from "../../entities/users/model";

export default createStore({
  modules: {
    user: userModule,
  },
});

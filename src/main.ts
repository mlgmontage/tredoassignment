import { createApp } from "vue";
import "./app/styles/style.css";
import App from "./app/App.vue";
import store from "./app/store";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faStar } from "@fortawesome/free-solid-svg-icons";

library.add(faStar);

createApp(App).use(store).mount("#app");

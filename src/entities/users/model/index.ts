import data from "../../../shared/api/data.json";
import _ from "lodash";

const user = {
  state: () => ({
    users: [],
    view: "table", // "table" | "card"
    loading: true,
    filters: {
      order: "asc", // "asc" | "desc"
      sort: "name", // "name" | "age" | "id"
    },
  }),
  mutations: {
    setusers(state, users) {
      state.users = users;
      state.loading = false;
    },
    setview(state, mode) {
      state.view = mode;
    },
    setorder(state, order) {
      state.filters.order = order;
    },
    setsort(state, by) {
      state.filters.sort = by;
    },
    toggleFavorite(state, id) {
      const i = state.users.findIndex((u) => u.id == id);
      state.users[i].favourite = !state.users[i].favourite;
      console.log(id, i, state.users[i]);
    },
  },
  actions: {
    // NOTE: Simlating backend REST api
    fetchusers({ commit }) {
      setTimeout(() => {
        commit("setusers", data);
      }, 400);
    },
  },
  getters: {
    filteredUsers(state) {
      return _.orderBy(
        state.users,
        [state.filters.sort],
        [state.filters.order]
      );
    },
  },
};

export default user;
